<?php

class entity_menu_links_fieldable_default_current_mlid extends views_plugin_argument_default{
  
  function get_argument() {
    $item = menu_link_get_preferred();
    return isset($item['mlid']) ? $item['mlid'] : NULL;
  }
}
