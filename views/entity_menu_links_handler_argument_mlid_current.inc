<?php

class entity_menu_links_handler_argument_mlid_current extends views_handler_argument{
  function default_actions($which = NULL){
    $defaults = array();

    $defaults['current_mlid'] = array(
      'title' => t('Provide current mlid, if available, hide if not'),
      'method' => 'mlid_if_available',
      'form method' => 'default_argument_form',
      'has default argument' => TRUE,
      'default only' => TRUE, // this can only be used for missing argument, not validation failure
    );
    
    if ($which) {
      if (!empty($defaults[$which])) {
        return $defaults[$which];
      }
    }
    else {
      return $defaults;
    }
  }
  
  function mlid_if_available(){
    $item = menu_link_get_preferred();
    return isset($item['mlid']) ? $item['mlid'] : $this->default_not_found();
    //return isset($item['mlid']) ? TRUE : FALSE;
  }

  function get_default_argument() {
    $item = menu_link_get_preferred();
    if (isset($item['mlid'])) {
      return $item['mlid'];
    }
  }

  function query($group_by = FALSE) {
    $v = $this->get_value();
    return parent::query($group_by);
  }
}

