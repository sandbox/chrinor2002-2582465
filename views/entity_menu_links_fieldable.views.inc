<?php

function entity_menu_links_fieldable_views_data_alter(&$data){
  $data['menu_links']['mlid_current']['title'] = t('Current Menu link');
  $data['menu_links']['mlid_current']['help'] = t('Tests a call to menu_get_item() against the menu items.');
  //$data['menu_links']['mlid_current']['filter']['handler'] = 'entity_menu_links_handler_filter_mlid_current';
  $data['menu_links']['mlid_current']['argument']['field'] = 'mlid';
  $data['menu_links']['mlid_current']['argument']['handler'] = 'entity_menu_links_handler_argument_mlid_current';
}


/**
 * Implements hook_views_plugins().
 */
function entity_menu_links_fieldable_views_plugins() {
  return array(
    'module' => 'entity_menu_links_fieldable', // This just tells our themes are elsewhere.
    'argument default' => array(
      'menu_link' => array(
        'title' => t('Current Menu link ID'),
        'handler' => 'entity_menu_links_fieldable_default_current_mlid'
      ),
    ),
  );
}
