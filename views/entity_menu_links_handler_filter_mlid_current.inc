<?php

class entity_menu_links_handler_filter_mlid_current extends views_handler_filter{
  function option_definition() {
    $options = parent::option_definition();

    return $options;
  }
  function value_form(&$form, &$form_state) {
    parent::value_form($form, $form_state);
  }

  function operators() {
    $operators = array(
      '=' => array(
        'title' => t('Is current page'),
        'method' => 'op_simple',
        'short' => t('is current'),
        'values' => 1,
      ),
      '!=' => array(
        'title' => t('Is not current page'),
        'method' => 'op_simple',
        'short' => t('is not current'),
        'values' => 1,
      ),
    );

    return $operators;
  }
  /**
   * Provide a list of all the numeric operators
   */
  function operator_options($which = 'title') {
    $options = array();
    foreach ($this->operators() as $id => $info) {
      $options[$id] = $info[$which];
    }

    return $options;
  }

  function operator_values($values = 1) {
    $options = array();
    foreach ($this->operators() as $id => $info) {
      if ($info['values'] == $values) {
        $options[] = $id;
      }
    }

    return $options;
  }
  
  function op_simple($field) {
    $this->query->add_where($this->options['group'], $field, $this->value['value'], $this->operator);
  }

  function query() {
    $this->ensure_my_table();
    $field = "$this->table_alias.$this->real_field";
    $item = menu_get_item();
    dsm(isset($item['mlid']), 'isset');
    if(isset($item['mlid'])){
      $this->value['value'] = $item['mlid'];

      $info = $this->operators();
      if (!empty($info[$this->operator]['method'])) {
        $this->{$info[$this->operator]['method']}($field);
      }
    }else{
      // -1 is used to enforce a false where needed
      $this->query->add_where($this->options['group'], '1', 0, '=');
    }
  }

  function admin_summary() {
    if ($this->is_a_group()) {
      return t('grouped');
    }
    if (!empty($this->options['exposed'])) {
      return t('exposed');
    }

    $options = $this->operator_options('short');
    $output = check_plain($options[$this->operator]);
    //$output .= '';
    return $output;
  }
}

